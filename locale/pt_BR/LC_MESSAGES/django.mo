��          �      �       0     1     F     T     d  0   m     �     �  	   �  5   �  
   �  )   �  :   )  j  d     �     �     �       2        F     N     a  ?   n     �  +   �  2   �     
                           	                          Brazilian Portuguese Building Name Day of Schedule End time Ending time must be higher than the starting one English Room Capacity Room Name Room Name: %s - Building Name: %s - Room Capacity: %s Start time Starting and Ending time cant be the same This room has already been scheduled for use at this time. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-08-08 15:16-0300
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Portugues Brasileiro Nome do Prédio Dia de Agendamento Hora de Fim O horário final não pode ser anterior ao inicial Inglês Capacidade da Sala Nome da Sala Nome da Sala: %s - Nome do Prédio: %s - Capacidade da Sala: %s Hora de Início Hora final e Inicial não podem ser a mesma Esta sala já está sendo utilizada neste horário 