Demo @ https://luizalabs-demo.herokuapp.com/pt-br/

---

## Requisitos

Esta api foi desenvolvida utilizando python 3.6 e banco de dados Mysql.

---

## Deploy Local

Para rodar este aplicativo em sua máquina local será necessário clonar o repositório em:
`
$ git clone https://fdmatte@bitbucket.org/fdmatte/heroku-labs-app.git
`
Após criar sua virtual enviroment e instalar as dependências através dos arquivo requirements.tx

Realize os processos collectstatic:
`
$ python manage.py collectstatic
`
Atualize as informações do banco de dados a suas preferências no arquivo settings.py:
Caso queira utilizar o banco de produção já configurado agora basta apenas rodar sua aplicação localmente.

`
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',  # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': '',  # Or path to database file if using sqlite3.
        'USER': '',  # Not used with sqlite3.
        'PASSWORD': '',  # Not used with sqlite3.
        'HOST': '',  # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',  # Set to empty string for default. Not used with sqlite3.
        'OPTIONS': {
            'sql_mode': 'traditional',
        }
    }
}

DATABASES['default'] = dj_database_url.config(default='mysql://try5ui8efebdkp8j:h7thus22hilrd6ah@a5s42n4idx9husyc.cbetxkdyhwsb.us-east-1.rds.amazonaws.com:3306/pb00116wfhmuc7xu')
`

---

## Deploy Heroku

Após clonar o repositório
`
$ git clone https://fdmatte@bitbucket.org/fdmatte/heroku-labs-app.git
`
Crie uma nova aplicação no Heroku
`
$ heroku create
`
Crie uma nova Provisão do JAWS DB MYSQL e atualize o link de conexão no arquivo settings.py
`
DATABASES['default'] = dj_database_url.config(default='**SEULINKAQUI**')
`
Após salve e comite as alterações
`
$ git add .
$ git commit -a -m "Mysql link update"
$ git push heroku master
`
Com as dependencias instaladas realize os processos de migrate, collectstatic e createsuperuser
`
$ heroku run python manage.py migrate
$ heroku run python manage.py collectstatic
$ heroku run python manage.py createsuperuser
`
Agora basta executar a aplicação
`
$ heroku open
`
