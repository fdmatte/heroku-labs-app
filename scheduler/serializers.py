from rest_framework import serializers
from .models import *
from django.utils.translation import ugettext_lazy as _

class RoomSerializer(serializers.ModelSerializer):

    class Meta:

        model = Room
        fields = '__all__'

class ScheduleSerializer(serializers.ModelSerializer):

    class Meta:

        model = Schedule
        fields = '__all__'
        read_only_fields = ('date_scheduled',)

    def validate(self, data):
        if data['end_time'] == data['start_time']:
            raise serializers.ValidationError(_('Starting and Ending time cant be the same'))

        if data['end_time'] <= data['start_time']:
            raise serializers.ValidationError(_('Ending time must be higher than the starting one'))

        if self.instance:
            schedules = Schedule.objects.exclude(pk=self.instance.id).filter(room=data['room'],day=data['day'])
        else:
            schedules = Schedule.objects.filter(room=data['room'],day=data['day'])

        if schedules.exists():
            for schedule in schedules:
                if self.check_collision(schedule.start_time, schedule.end_time, data['start_time'], data['end_time']):
                    raise serializers.ValidationError(_("This room has already been scheduled for use at this time."))

        return data

    def check_collision(self, booked_starttime, booked_endtime, new_schedule_start, new_schedule_end):
        collision = False
        #Verify time collisions so one room cant have the same time booked twice
        if (new_schedule_start >= booked_starttime and new_schedule_start <= booked_endtime) or (new_schedule_end >= booked_starttime and new_schedule_end <= booked_endtime):
            collision = True
        elif new_schedule_start <= booked_starttime and new_schedule_end >= booked_endtime:
            collision = True

        return collision
