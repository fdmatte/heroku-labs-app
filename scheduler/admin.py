from django.contrib import admin
from scheduler import models

# Register your models here.
@admin.register(models.Room)
class RoomAdmin(admin.ModelAdmin):

    list_display = ('name', 'building', 'capacity')
    search_fields = ['name', 'building']

@admin.register(models.Schedule)
class ScheduleAdmin(admin.ModelAdmin):

    list_display = ('contact', 'day','start_time','end_time','room')
    search_fields = ['contact', 'room']
    readonly_fields = ('date_scheduled',)
