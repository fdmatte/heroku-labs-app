from django.db import models
from django.core.exceptions import ValidationError
import datetime
from django.utils.translation import ugettext_lazy as _


# Create your models here.
class Room(models.Model):

    class Meta:
        db_table = 'scheduler_room'

    name = models.CharField(_(u'Room Name'),  unique=True, max_length=255)
    building = models.CharField(_(u'Building Name'), max_length=255)
    capacity = models.IntegerField(_(u'Room Capacity'))

    def __unicode__(self):
        return _(u"Room Name: %s - Building Name: %s - Room Capacity: %s") % (self.name, self.building, self.capacity)

    def __str__(self):
        return _(u"Room Name: %s - Building Name: %s - Room Capacity: %s") % (self.name, self.building, self.capacity)

class Schedule(models.Model):

    class Meta:
        db_table = 'scheduler_schedule'

    room = models.ForeignKey('Room' , on_delete=models.PROTECT)
    contact = models.CharField(max_length=255)
    day = models.DateField(_(u'Day of Schedule'), default=datetime.date.today)
    start_time = models.TimeField(_(u'Start time'),default=datetime.time(16, 00))
    end_time = models.TimeField(_(u'End time'),default=datetime.time(16, 00))
    date_scheduled = models.DateTimeField(default=datetime.datetime.now())

    def __unicode__(self):
        return _(u"Room Name: %s - Building Name: %s - Room Capacity: %s") % (self.title, self.building, self.capacity)


    def check_collision(self, booked_starttime, booked_endtime, new_schedule_start, new_schedule_end):
        collision = False
        #Verify time collisions so one room cant have the same time booked twice
        if (new_schedule_start >= booked_starttime and new_schedule_start <= booked_endtime) or (new_schedule_end >= booked_starttime and new_schedule_end <= booked_endtime):
            collision = True
        elif new_schedule_start <= booked_starttime and new_schedule_end >= booked_endtime:
            collision = True

        return collision

    def clean(self):

        if self.end_time == self.start_time:
            raise ValidationError(_('Starting and Ending time cant be the same'))

        if self.end_time <= self.start_time:
            raise ValidationError(_('Ending time must be higher than the starting one'))

        schedules = Schedule.objects.filter(room=self.room,day=self.day)
        if schedules.exists():
            for schedule in schedules:
                if self.check_collision(schedule.start_time, schedule.end_time, self.start_time, self.end_time):
                    raise ValidationError(_("This room has already been scheduled for use at this time."))
