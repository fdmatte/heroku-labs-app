from django.urls import re_path

from scheduler.views import *

urlpatterns = [
 re_path(r'^rooms/$', RoomList.as_view(), name='Rooms List'),
 re_path(r'^rooms/<slug:pk>/$', RoomDetail.as_view(), name='Rooms List'),
 re_path(r'^schedules/$', ScheduleList.as_view(), name='Schedules List'),
 re_path(r'^schedules/<slug:pk>/$', ScheduleDetail.as_view(), name='Schedules Detail'),

]
