from rest_framework import generics, filters
from .models import *
from .serializers import *

class RoomList(generics.ListCreateAPIView):

    queryset = Room.objects.all()
    serializer_class = RoomSerializer
    filter_backends = (filters.SearchFilter,filters.OrderingFilter,)
    search_fields = ('name','building',)
    ordering_fields  = ('name','building','capacity',)

    def get_queryset(self):
            queryset = Room.objects.all()
            name = self.request.query_params.get('name', None)
            building = self.request.query_params.get('building', None)
            capacity = self.request.query_params.get('capacity', None)

            if name is not None:
                queryset = queryset.filter(name=name)

            if building is not None:
                queryset = queryset.filter(building=building)

            if capacity is not None:
                queryset = queryset.filter(capacity__gte=capacity)


            return queryset

class RoomDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Room.objects.all()
    serializer_class = RoomSerializer

class ScheduleList(generics.ListCreateAPIView):

    queryset = Schedule.objects.all()
    serializer_class = ScheduleSerializer
    filter_backends = (filters.SearchFilter,filters.OrderingFilter,)
    search_fields = ('day','contact','room__name',)
    ordering_fields  = ('day','contact','room__name',)

    def get_queryset(self):
            queryset = Schedule.objects.all()
            day = self.request.query_params.get('day', None)
            contact = self.request.query_params.get('contact', None)
            room = self.request.query_params.get('room', None)

            if contact is not None:
                queryset = queryset.filter(contact=contact)

            if day is not None:
                queryset = queryset.filter(day=day)

            if room is not None:
                queryset = queryset.filter(room__name=room)



            return queryset

class ScheduleDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Schedule.objects.all()
    serializer_class = ScheduleSerializer
