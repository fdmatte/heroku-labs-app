from django.http import HttpResponse
from django.views.generic.base import TemplateView

# Create your views here.
class HomeView(TemplateView):

    template_name = "portal/home.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        return context
