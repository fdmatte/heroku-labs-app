from django.urls import re_path

from portal.views import *

urlpatterns = [
 re_path(r'^$', HomeView.as_view(), name='home'),
]
